# ANDROID SPARSE IMAGE TOOLKIT (STANDALONE)
# EXTRACTED FROM THE ANDROID OPEN SOURCE PROJECT.
# Copyright (C) 2012 The Android Open Source Project
# Copyright (C) 2015 Devoleda Organisation
# Copyright (C) Ethan levy <eitanlevy97@yandex.com>

SIMG2IMG = simg2img
IMG2SIMG = img2simg
APPEND2SIMG = append2simg

CFLAGS ?= -O2 -Werror
INCLUDEDIR = -I./include -I.

LIBRARY_OBJS = backed_block.o \
		output_file.o \
		sparse.o \
		sparse_crc32.o \
		sparse_err.o \
		sparse_read.o
SIMG2IMG_OBJS = simg2img.o
IMG2SIMG_OBJS = img2simg.o
APPEND2SIMG_OBJS = append2simg.o

LINK_LIBS = -lz

.PHONY: all clean
all: $(SIMG2IMG) $(IMG2SIMG) $(APPEND2SIMG)
$(SIMG2IMG): $(SIMG2IMG_OBJS) $(LIBRARY_OBJS)
	@echo "    LD $@"
	@$(CC) $^ -o $@ $(LINK_LIBS)

$(IMG2SIMG): $(IMG2SIMG_OBJS) $(LIBRARY_OBJS)
	@echo "    LD $@"
	@$(CC) $^ -o $@ $(LINK_LIBS)

$(APPEND2SIMG): $(APPEND2SIMG_OBJS) $(LIBRARY_OBJS)
	@echo "    LD $@"
	@$(CC) $^ -o $@ $(LINK_LIBS)

.c.o:
	@echo "    CC $@"
	@$(CC) $(CFLAGS) $(INCLUDEDIR) -c $< -o $@

clean:
	@-rm *.o $(SIMG2IMG) $(IMG2SIMG) $(APPEND2SIMG)